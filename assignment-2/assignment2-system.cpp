#include "mbed.h"
DigitalOut led1(LED1), led2(LED2), led3(LED3), led4(LED4);
DigitalOut pp1(p5), pp2(p6), pp3(p7), pp4(p8);

DigitalOut trig(p10);
DigitalIn echo(p11);
Timer timer;
long duration,distance_cm;
unsigned int on = 0;

 
 void SetPortValues(char *myValues){
    pp1 = led1 = myValues[0] - 48;
    pp2 = led2 = myValues[1] - 48;
    pp3 = led3 = myValues[2] - 48;
    pp4 = led4 = myValues[3] - 48;
}
char* forwardArr[] = {"1000", "1100", "0100", "0110","0010", "0011","0001", "1001"};
char* backwardArr[] = {"1000", "1001", "0001", "0011", "0010", "0110", "0100", "1100"};
enum control{
    forward,
    backward
}stepper_direction = backward;

long echo_duration() {
timer.reset();  //reset timer
trig=0;   // trigger low 
wait_us(2); //  wait 
trig=1;   //  trigger high
wait_us(10);
trig=0;  // trigger low
led1 = 1;
while(!echo); // start pulseIN
timer.start();
while(echo);
timer.stop();
return timer.read_us(); 
 
}
 
//return distance in cm 
long distance(){
duration = echo_duration();
distance_cm = (double) (duration/2) * 0.034;
return distance_cm; 
}

Serial pc(USBTX, USBRX); // tx, rx
void attime() {
   stepper_direction?SetPortValues(backwardArr[++on%8]):SetPortValues(forwardArr[++on%8]);
}

Ticker timerSetMotorFreq;

void ReadDistance(){
    double s = 0;
    timerSetMotorFreq.detach();
    long dis = distance(); 
    if (dis > 65) dis = 65;     // 0.001->5; 0.01->400
    if (dis < 5) dis = 5; 
    stepper_direction = (dis > 30) ? forward : backward ;
    pc.printf("distance  %d  \n",dis);
    dis = (dis >= 30) ? 65 - dis : dis;  
    s = (double)dis*0.000022 + 0.001;
    timerSetMotorFreq.attach(&attime, s);
}


int main() {
    Ticker timerCaptureSensorValue;
    timerCaptureSensorValue.attach(ReadDistance, 1);
    while(1) {
    }
}
