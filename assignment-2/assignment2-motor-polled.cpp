#include "mbed.h"
DigitalOut led1(LED1), led2(LED2), led3(LED3), led4(LED4);
DigitalOut pp1(p5), pp2(p6), pp3(p7), pp4(p8);

char* forwardArr[] = {"1000", "1100", "0100", "0110","0010", "0011","0001", "1001"};
char* backwardArr[] = {"1000", "1001", "0001", "0011", "0010", "0110", "0100", "1100"};
enum control{
    forward,
    backward
}stepper_direction = backward;

Ticker timer;
unsigned int on = 0;
void SetPortValues(char *myValues){
    pp1 = led1 = myValues[0] - 48;
    pp2 = led2 = myValues[1] - 48;
    pp3 = led3 = myValues[2] - 48;
    pp4 = led4 = myValues[3] - 48;
}

Serial pc(USBTX, USBRX); // tx, rx
void attime() {
   stepper_direction?SetPortValues(backwardArr[++on%8]):SetPortValues(forwardArr[++on%8]);
}

int main() {
  timer.attach(&attime, 0.001);
    while(1) {
        char c = pc.getc();
        if (c == 'f')
            stepper_direction = forward;
        if(c == 'b')
            stepper_direction = backward;
        pc.printf("dir = %d \r",stepper_direction);
    }
}