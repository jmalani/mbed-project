#include "mbed.h"

DigitalOut trig(p10); // To generate trigger for Sonar
DigitalIn echo(p11); // To capture echo edge on I/O interrupt
Timer timer;
long duration,distance_cm;

long echo_duration() {
  timer.reset();  //reset timer
  trig=0;   // trigger low 
  wait_us(2); //  wait 
  trig=1;   //  trigger high
  wait_us(10);
  trig=0;  // trigger low
  while(!echo); // start pulseIn
  timer.start();
  while(echo);
  timer.stop();
  return timer.read_us(); 
}
 
//return distance in cm 
long distance(){
  duration = echo_duration();
  distance_cm = (duration/2)/29.1  ;
  return distance_cm; 
}

Serial pc(USBTX, USBRX); // tx, rx

int main() {
    while(1) {
      long dis = distance(); 
      pc.printf("distance  %d  \n",dis);
      wait(0.4); // 0.4 sec 
    }
}