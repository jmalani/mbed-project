#include "mbed.h"

DigitalOut trig(p10); // To generate trigger for Sonar
InterruptIn echo(p5); // To capture echo edge on I/O interrupt
Timer timer;
long duration, distance_cm;

Serial pc(USBTX, USBRX); // tx, rx
 
void echo_duration() {
    trig=0;   // trigger low 
    wait_us(2); //  wait 
    trig=1;   //  trigger high
    wait_us(10);
    trig=0;  // trigger low
}
 
//return distance in cm 
void distance(){
    timer.reset();  //reset timer
    timer.start();
    while(echo);
    timer.stop();
    duration =  timer.read_us(); // Pulse width measured
    distance_cm = (double)(duration/2)*0.034  ; 
    pc.printf("D=%d\n",distance_cm);
}

int main() {
    echo.rise(&distance);
    while(1) {
      echo_duration(); 
      wait(0.4); // 0.4 sec 
    }
}